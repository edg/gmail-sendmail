#!/usr/bin/python3

import sys
import os.path
import base64
import configparser
import email
import smtplib

from googleapiclient.discovery import build
from httplib2 import Http
import oauth2client.file


def read_config(path):
    path = os.path.expanduser(path)
    cfg = configparser.ConfigParser(default_section="defaults")
    cfg.read(path)
    return cfg


def get_account(cfg, name):
    if name is None:
        name = cfg.get("base", "default_account")

    section = dict(cfg.items("account {}".format(name)))
    return section


def sendmail_smtp(account, msg):
    token_path = os.path.expanduser(account['token_path'])
    creds = oauth2client.file.Storage(token_path).get()
    http = creds.authorize(Http())
    creds.refresh(http)
    auth = 'user={}\1auth=Bearer {}\1\1'.format(
        account['username'], creds.access_token
    )

    smtp = smtplib.SMTP_SSL()
    smtp.connect(host="smtp.gmail.com", port=465)
    smtp.ehlo()
    # smtp.starttls()
    smtp.docmd("AUTH", "XOAUTH2 {}".format(
        base64.b64encode(auth.encode("utf-8")).decode("utf-8")
    ))
    smtp.send_message(msg)


def sendmail_http(account, msg):
    token_path = os.path.expanduser(account['token_path'])
    creds = oauth2client.file.Storage(token_path).get()
    service = build('gmail', 'v1', http=creds.authorize(Http()))
    service.users().messages().send(userId="me", body={
        "raw": base64.urlsafe_b64encode(msg.as_bytes()).decode("utf-8"),
    }).execute()


def sendmail(account, msg):
    return {
        'smtp': sendmail_smtp,
        'http': sendmail_http,
    }[account['type']](account, msg)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument("-c", "--config",
                        help="config file default: %(default)s",
                        default="~/.config/gmail-sendmail/config")
    parser.add_argument("-a", "--account", help="account name")
    parser.add_argument("-f", help="sender")
    parser.add_argument("recipient", nargs="*")

    args = parser.parse_args()

    try:
        cfg = read_config(args.config)
        account = get_account(cfg, args.account)
        buf = sys.stdin.buffer.read()
        msg = email.message_from_bytes(buf)
        sendmail(account, msg)
    except Exception as e:
        sys.stderr.write("Error: {}\n".format(e))
        raise e
