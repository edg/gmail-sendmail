# gmail-sendmail

Read mail from stdin and send it using Gmail API or SMTP with SASL XOAUTH2
authentication.

## Usage

Configuration file

```
[base]
default_account = emanuele

[account emanuele_http]
type = http
username = myaccount@gmail.com
token_path = ~/.emanuele_token.json

[account emanuele_smtp]
type = smtp
username = myaccount@gmail.com
token_path = ~/.emanuele_token.json
```

Synopsys:

```
usage: gmail-sendmail.py [-h] [-c CONFIG] [-a ACCOUNT]

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        config file default: ~/.config/gmail-sendmail/config
  -a ACCOUNT, --account ACCOUNT
                        account name
```

Send mail:

```
$ echo -e "from:me@gmail.com\nto:you@gmail.com\nsubject:hello\n\nhello" | python3 gmail-sendmail.py -a emanuele -c ~/.gmail-sendmail.config
```

Mutt configuration

```
set sendmail = "gmail-sendmail.py -a emanuele"
```

# Notes

It seems that Gmail threading has some problems when a reply is sent via HTTP API.
