from oauth2client import file, client, tools


if __name__ == '__main__':
    scopes = [
        'https://www.googleapis.com/auth/gmail.send',
    ]

    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('secret.json', scopes)
        creds = tools.run_flow(flow, store)
